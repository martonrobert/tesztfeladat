<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;

class Companies extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'logo', 'email', 'website'];
}
