<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Employee;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $employee = Employee::paginate(10);
      $companies = DB::table('companies')->select('id', 'name')->get();
      return view('index_employee', compact('employee'),compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = DB::table('companies')->select('id', 'name')->get();
        return view('create_employee',compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validatedData = $request->validate([
       'first_name' => 'required|max:255',
       'last_name' => 'required|max:255',
       'company' => 'required',
       'email' => 'nullable|email|max:100',
       'phone' => 'nullable|numeric',
       ]);
       $employee = Employee::create($validatedData);

       return redirect('/employee')->with('success', 'Employer is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $employee = Employee::findOrFail($id);
      $companies = DB::table('companies')->select('id', 'name')->get();
      return view('edit_employee', compact('employee'),compact('companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $validatedData = $request->validate([
       'first_name' => 'required|max:255',
       'last_name' => 'required|max:255',
       'company' => 'required',
       'email' => 'nullable|email|max:100',
       'phone' => 'nullable|numeric',
       ]);

      Employee::whereId($id)->update($validatedData);

      return redirect('/employee')->with('success', 'Employer is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $employee = Employee::findOrFail($id);
      $employee->delete();

      return redirect('/employee')->with('success', 'Employer is successfully deleted');
    }
}
