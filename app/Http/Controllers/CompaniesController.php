<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Companies;
class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Companies::paginate(10);
        return view('index_companies', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create_companies');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validatedData = $request->validate([
       'name' => 'required|max:255',
       'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:min_width=100,min_height=100',
       'email' => 'nullable|email|max: 100',
       'website' => 'max: 100',
     ]);

     $imageName = time().'.'.request()->logo->getClientOriginalExtension();
     $validatedData['logo'] = $imageName;
     request()->logo->move(storage_path('app\public\images'), $imageName);

     $company = Companies::create($validatedData);

     return redirect('/companies')->with('success', 'Company is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $company = Companies::findOrFail($id);
      return view('edit_companies', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasFile('logo')){
          $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'nullable|email|max: 100',
            'website' => 'max: 100',
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:min_width=100,min_height=100',
          ]);
          $imageName = time().'.'.request()->logo->getClientOriginalExtension();
          $validatedData['logo'] = $imageName;
          request()->logo->move(storage_path('app\public\images'), $imageName);

          //xxx need to delete the old image
        }else{
          $validatedData = $request->validate([
           'name' => 'required|max:255',
           'email' => 'nullable|email|max: 100',
           'website' => 'max: 100',
          ]);
        }


       $company = Companies::whereId($id)->update($validatedData);

       return redirect('/companies')->with('success', 'Company is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $company = Companies::findOrFail($id);
      $company->delete();

      return redirect('/companies')->with('success', 'Company is successfully deleted');
    }
}
