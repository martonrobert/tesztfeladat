@extends('layout_companies')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Edit Book
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('employee.update', $employee->id) }}">
          <div class="form-group">
              @csrf
              @method('PATCH')
              <label for="first_name">First name</label>
              <input type="text" class="form-control" name="first_name" value="{{$employee->first_name}}"/>
          </div>
          <div class="form-group">
              <label for="last_name">Last name</label>
              <input type="text" class="form-control" name="last_name" value="{{$employee->last_name}}"/>
          </div>
          <div class="form-group">
              <label for="company">Company</label>
              <select class="form-control" name="company">
                @foreach($companies as $company)
                  <option value="{{$company->id}}" @if($employee->id == $company->id) selected="selected" @endif>{{$company->name}}</option>
                @endforeach
              </select>
          </div>
          <div class="form-group">
              <label for="email">Email</label>
              <input type="text" class="form-control" name="email" value="{{$employee->email}}"/>
          </div>
          <div class="form-group">
              <label for="phone">Phone</label>
              <input type="text" class="form-control" name="phone" value="{{$employee->phone}}"/>
          </div>
          <button type="submit" class="btn btn-primary">Update Employer</button>
      </form>
  </div>
</div>
@endsection
