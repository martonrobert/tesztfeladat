@extends('layout_companies')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div><br />
  @endif
  <a href="{{ route('employee.create') }}" class="btn btn-warning">Create Employee</a>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Name</td>
          <td>Company</td>
          <td>Email</td>
          <td>Phone</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($employee as $employer)
        <tr>
            <td>{{$employer->id}}</td>
            <td>{{$employer->first_name}} {{$employer->last_name}}</td>
            <td>
              @foreach($companies as $company)
                @if($employer->company == $company->id)
                 {{$company->name}}
                @endif
              @endforeach
            </td>
            <td>{{$employer->email}}</td>
            <td>{{$employer->phone}}</td>
            <td><a href="{{ route('employee.edit',$employer->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('employee.destroy', $employer->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
  {{ $employee->links() }}
<div>
@endsection
