@extends('layout_companies')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Add Company
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('companies.store') }}" enctype="multipart/form-data">
          <div class="form-group">
              @csrf
              <label for="name">Company Name:</label>
              <input type="text" class="form-control" name="name"/>
          </div>
          <div class="form-group">
              <label for="email">Email</label>
              <input type="text" class="form-control" name="email"/>
          </div>
          <div class="form-group">
              <label for="website">Website:</label>
              <input type="text" class="form-control" name="website"/>
          </div>
          <div class="form-group">
              <label for="logo">Logo:</label>
              <input type="file" class="form-control" name="logo">
          </div>
          <button type="submit" class="btn btn-primary">Create Company</button>
      </form>
  </div>
</div>
@endsection
