@extends('layout_companies')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div><br />
  @endif
  <a href="{{ route('companies.create') }}" class="btn btn-warning">Create Company</a>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Company Name</td>
          <td>Email</td>
          <td>Website</td>
          <td>Logo</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($companies as $company)
        <tr>
            <td>{{$company->id}}</td>
            <td>{{$company->name}}</td>
            <td>{{$company->email}}</td>
            <td>{{$company->website}}</td>
            <td><img src="{{storage_path('app/public/images')}}/{{$company->logo}}" /></td>
            <td><a href="{{ route('companies.edit',$company->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('companies.destroy', $company->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
  {{ $companies->links() }}
<div>
@endsection
