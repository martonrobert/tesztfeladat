@extends('layout_companies')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Edit Book
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('companies.update', $company->id) }}" enctype="multipart/form-data">
          <div class="form-group">
              @csrf
              @method('PATCH')
              <label for="name">Company Name:</label>
              <input type="text" class="form-control" name="name" value="{{$company->name}}"/>
          </div>
          <div class="form-group">
              <label for="email">Email:</label>
              <input type="text" class="form-control" name="email" value="{{$company->email}}"/>
          </div>
          <div class="form-group">
              <label for="website">Website:</label>
              <input type="text" class="form-control" name="website" value="{{$company->website}}"/>
          </div>
          <div class="form-group">
              <label for="logo">Logo:</label>
              <input type="file" class="form-control" name="logo" value="{{storage_path('app/public/images')}}/{{$company->logo}}">
          </div>
          <button type="submit" class="btn btn-primary">Update Company</button>
      </form>
  </div>
</div>
@endsection
