<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Laravel 5.8</title>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
</head>
<body>
  <div class="container">
    <a href="/companies" class="btn btn-success">Companies</a>
    <a href="/employee" class="btn btn-warning">Employee</a>
    @yield('content')
    @section('content')
    <div class="container">
      <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card">
                  <div class="card-body">
                  <p>{{ trans('sentence.welcome')}}</p>
                  </div>
              </div>
          </div>
      </div>
    </div>
    @endsection
  </div>
  <script src="{{ asset('js/app.js') }}" type="text/js"></script>
</body>
</html>
